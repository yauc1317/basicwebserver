import loginRoute from "./setting/login/route/loginRoute";
import optionRoute from "./setting/option/routes/optionRoute";
import functionalityRoute from "./setting/functionality/route/functionalityRoute";
import profileRoute from "./setting/profile/routes/profileRoutes";
import userRoute from "./setting/user/route/userRoute";
import sectionRoutes from "./setting/section/routes/sectionRoutes";
import constantRoute from "./setting/constant/routes/constantRoutes";


const express = require('express');
const app = express();
//todas la rutas definidas
app.use(process.env.API_URL+'/login', loginRoute);
app.use(process.env.API_URL+'/option', optionRoute);
app.use(process.env.API_URL+'/functionality', functionalityRoute);
app.use(process.env.API_URL+'/profile', profileRoute);
app.use(process.env.API_URL+'/section', sectionRoutes);
app.use(process.env.API_URL+'/user', userRoute);
app.use(process.env.API_URL+'/constant', constantRoute);


module.exports = app;
