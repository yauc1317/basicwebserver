
import express, {Application} from 'express';
//import mongoose from 'mongoose'; 


class Server{
    public app: Application;
    public http: any;
    public io: any;
    public documents: any[] = [];

    constructor(){
    this.app = express();
    this.http = require('http').Server(this.app);
    this.io = require('socket.io')(this.http);
    
   
    this.config();
    this.routes();
    this.routesWebSocket();
    //this.connectionDbMongo();
    }

    config():void{
        this.app.set('port', process.env.PORT || 3000); 
        // necesario para recuperar body del Request      
        this.app.use(express.json()); //parse application/json
        this.app.use(express.urlencoded({extended: false})); //parse application/x-www-form-urlencoded
        //permitir el dominio cruzado (habilitar CORS)
        this.app.use(function(req, res, next){
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          next();
        });
    }

    routes():void{         
        this.app.use(require('./generalRoutes'));
    }

    routesWebSocket():void{         
      require('./generalRouteSocket')(this.io); // rutas de websockets
    }

 
    

    /*connectionDbMongo(){
      mongoose.connect('mongodb://localhost:27017/adminfutsal', (err: any,res: any) => {
        if(err) {console.log('error al conectar base de datos'); throw err;}
        console.log('Base de Datos Mongo Online');
      });
    }
    */

    star():void{
      const port = this.app.get('port');
        this.http.listen(this.app.get('port'), function(){
            console.log('servidor iniciado y escuchando en el puerto: ',port);
          });
        
        
    }  
}
const ser = new Server();
ser.star();
