import { Request, Response } from 'express';
import optionValueDao from '../dao/optionValueDao'
import { SystemOptionValue } from '../../../model/SystemOptionValue';
import { Constants } from '../../../constant/Constant';


class OptionValueController {
    public async listAllOption(req: Request, res: Response): Promise<any> {
        try {
            const { idOpt } = req.params;
            const list = await optionValueDao.listOptionValue(+idOpt);
            const { id } = req.params;
            // const list = await optionValueDao.listOptionValue(+id);
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }
    }

    public async listOptionByCode(req: Request, res: Response): Promise<any> {
        try { 
            const { codeOpt } = req.params;               
            const list = await optionValueDao.listOptionValueByCode(codeOpt);
            const {id} = req.params;
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }
    }

    public async insert(req: Request, res: Response): Promise<any> {
        console.log('entra a insert', req.body);
        try {
            const opt: SystemOptionValue = req.body;
            const rs: boolean = await optionValueDao.insert(opt);
            if (rs) {
                res.status(200).json({
                    status: Constants.SUCCESS,
                    message: 'insertado correctamente ',
                    data: true
                });
            }
        } catch (err) {
            console.log(err)
            res.status(500).json({
                status: Constants.FAIL,
                message: err.toString(),
                data: false
            });
        }
    }

    public async update(req: Request, res: Response): Promise<any> {
        console.log('entra a update', req.body);
        try {
            const opt: SystemOptionValue = req.body;
            console.log('*****objeto:', opt);
            const rs: boolean = await optionValueDao.update(opt);
            if (rs) {
                res.status(200).json({
                    status: Constants.SUCCESS,
                    message: 'actualizado correctamente ',
                    data: true
                });
            }
        } catch (err) {
            console.log(err)
            res.status(500).json({
                status: Constants.FAIL,
                message: err.toString(),
                data: false
            });
        }

    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { idOptVal } = req.params;
            console.log('delete optionvalue');
            console.log(req.params)
            const r: boolean = await optionValueDao.delete(+idOptVal);

            if (r) {
                res.json({
                    status: Constants.SUCCESS,
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: Constants.FAIL,
                    message: 'error no eliminado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: Constants.FAIL,
                message: 'error respuesta servidor no eliminado',
                data: false
            });
        }
    }
}

const optionValueController = new OptionValueController;
export default optionValueController;
