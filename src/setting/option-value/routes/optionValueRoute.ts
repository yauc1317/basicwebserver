import { Router } from "express";
import optionValueController from "../controller/optionValueController";

class OptionValueRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {     
        //estas rutas NO se estan utilizando, estan como sub-rutas de option   
        this.router.get('/list', optionValueController.listAllOption);
        this.router.post('/create', optionValueController.insert);
        this.router.put('/update', optionValueController.update);
        this.router.delete('/delete/:id', optionValueController.delete);
    }

}

export default new OptionValueRoutes().router;