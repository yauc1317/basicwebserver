import pool from '../../database';
import { Query } from '../../../query/query';
import { SystemEapb } from '../../../model/SystemEapb';
import { SystemConstant } from '../../../model/SystemConstant';
import { SystemInstitution } from '../../../model/SystemInstitution';
import { ENETRESET } from 'constants';


class ConstantDao {

    public async list(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_CONSTANT);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                const obj: SystemConstant = new SystemConstant();
                obj.idConstant = item.id_constant
                obj.idInst = item.id_inst
                obj.nameConstant = item.name_constant
                obj.valueConstant = item.value_constant
                obj.descConstant = item.desc_constant
                obj.nameInst = item.name_inst
                return obj;
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async insert(o: SystemConstant): Promise<any> {

        var res = false;
        try {
            var enter=null
            if(o.idInst!=undefined && o.idInst>0){
                enter=o.idInst
            }
            await pool.query(Query.INSERT_CONSTANT,
                [enter, o.nameConstant, o.valueConstant, o.descConstant]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async update(o: SystemConstant): Promise<any> {
        var res = false;
        try {
            var enter=null
            console.log(o)
            if(o.idInst!=undefined && o.idInst>0){
                enter=o.idInst
            }
            await pool.query(Query.UPDATE_CONSTANT,
                [enter, o.nameConstant, o.valueConstant, o.descConstant, o.idConstant]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }


    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_CONSTANT, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async listInst(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_CONSTANT_INST);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                const obj: SystemInstitution = new SystemInstitution();                
                obj.idInst = item.id_inst
                obj.nitInst = item.nit_inst
                obj.nameInst = item.name_inst
                return obj;
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }
}

const constantDao = new ConstantDao;
export default constantDao;
