import { Router } from "express";
import constantController from "../controller/constantController";

class ConstantRoute {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {        
        this.router.get('/list', constantController.list);
        this.router.get('/listInst', constantController.listInst);
        this.router.post('/constant', constantController.insert);
        this.router.put('/constant', constantController.update);
        this.router.delete('/constant/:id', constantController.delete);
               
    }

}

export default new ConstantRoute().router;