import { Request, Response } from 'express';
import constantDao from '../dao/constantDao'
import { SystemEapb } from '../../../model/SystemEapb';
import { Constants } from '../../../constant/Constant';
import { SystemConstant } from '../../../model/SystemConstant';


class ConstantController {

    public async list(req: Request, res: Response): Promise<any> {
        
        try {
            const list = await constantDao.list();
            res.status(200).json({
                status: Constants.SUCCESS,
                message : 'lista ',
                data: list
            });
        } catch (error) {           
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }    
    }

    
    public async insert(req: Request, res: Response): Promise<any> {
       
        try {
        const opt:SystemConstant = req.body;
        const rs: boolean = await constantDao.insert(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: Constants.FAIL,
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async update(req: Request, res: Response): Promise<any> {
        
        try {
        const opt:SystemConstant = req.body;
        const rs: boolean = await constantDao.update(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: Constants.FAIL,
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            const r: boolean = await constantDao.delete(+id);
            if (r) {
                res.json({
                    status: Constants.SUCCESS,
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: Constants.FAIL,
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: Constants.FAIL,
                message: err.toString(),
                data: false
            });
        }
    }

    public async listInst(req: Request, res: Response): Promise<any> {
        
        try {
            const list = await constantDao.listInst();
            res.status(200).json({
                status: Constants.SUCCESS,
                message : 'lista ',
                data: list
            });
        } catch (error) {           
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }    
    }
}
const constantController = new ConstantController;
export default constantController;
