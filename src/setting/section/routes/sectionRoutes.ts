import { Router } from "express";
import sectionController from "../controller/sectionController";

class SectionRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {        
        this.router.get('/list', sectionController.listSection);
        this.router.post('/section', sectionController.insert);
        this.router.put('/section', sectionController.update);
        this.router.delete('/section/:id', sectionController.delete);
               
    }

}

export default new SectionRoutes().router;