import { Request, Response } from 'express';
import sectionDao from '../dao/sectionDao'
import { SystemSection } from '../../../model/SystemSection';


class ServiceController {

    public async listSection(req: Request, res: Response): Promise<any> {
        
        try {
            const list = await sectionDao.listSection();
            res.status(200).json({
                status: 'SUCCESS',
                message : 'lista ',
                data: list
            });
        } catch (error) {           
            console.log(error.toString())
            res.status(500).json({
                status: 'FAIL',
                message: error.toString(),
                data: null
            });
        }    
    }

    
    public async insert(req: Request, res: Response): Promise<any> {
       
        try {
        const opt:SystemSection = req.body;
        const rs: boolean = await sectionDao.insert(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async update(req: Request, res: Response): Promise<any> {
        
        try {
        const opt:SystemSection = req.body;
        const rs: boolean = await sectionDao.update(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            const r: boolean = await sectionDao.delete(+id);
            if (r) {
                res.json({
                    status: 'SUCCESS',
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: 'FAIL',
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: 'FAIL',
                message: err.toString(),
                data: false
            });
        }
    }




}
const serviceController = new ServiceController;
export default serviceController;
