import pool from '../../database';
import { Query } from '../../../query/query';
import { SystemSection } from '../../../model/SystemSection';


class SectionDao {

    public async listSection(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_SECTION);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                const obj: SystemSection = new SystemSection();
                obj.idSection= item.id_section
                obj.nameSection = item.name_section
                obj.descSection = item.desc_section                
                return obj;
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async insert(o: SystemSection): Promise<any> {

        var res = false;
        try {
            await pool.query(Query.INSERT_SECTION,
                [   o.nameSection, o.descSection]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async update(o: SystemSection): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.UPDATE_SECTION,
                [ o.nameSection, o.descSection, o.idSection]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }


    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_SECTION, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }
}

const sectionDao = new SectionDao;
export default sectionDao;
