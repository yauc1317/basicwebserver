import {Router} from "express";
import userController from "../controller/userController";

class UserRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.get('/listUser', userController.listAllUser);
        this.router.post('/user',userController.insertUser);
        this.router.put('/user', userController.updateUser);
        this.router.delete('/user/:id',userController.delete);
        this.router.get('/optionValue',userController.listOptionValue);

    }
}
export default new UserRoutes().router;