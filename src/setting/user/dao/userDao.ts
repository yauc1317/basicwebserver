import pool from '../../database';
import { Query } from '../../../query/query';
import { UserModel } from '../../../model/UserModel';
import { SystemOptionValue } from '../../../model/SystemOptionValue';

class OptionDao {
    public async listAllUser(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_ALL_USER);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                const obj: UserModel = new UserModel();
                obj.idUser = item.id_user;
                obj.idSpeciality = item.id_speciality;
                obj.idProfile = item.id_profile;
                obj.nameUser = item.name_user;
                obj.lastNameUser = item.last_name_user;
                obj.identUser = item.ident_user;
                obj.emailUser = item.email_user;
                obj.activeUser = item.active_user;
                obj.phoneUser = item.phone_user;
                return obj;
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_USER, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
        }
        return res;
    }

    public async listOptionValue(codeOpt:  string): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_OPTION_VALUE_BY_CODE,[codeOpt]);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemOptionValue = new SystemOptionValue();
                    obj.idOptVal= item.id_opt_val
                    obj.idOpt= item.id_opt
                    obj.nameOptVal= item.name_opt_val
                    obj.descOptVal= item.desc_opt_val
                    obj.stateOptVal = item.state_opt_val
                    obj.codOptVal= item.cod_opt_val
                    return obj;
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }
        return list;
    }

}
const optionDao = new OptionDao;
export default optionDao;