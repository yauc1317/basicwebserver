import { Request, Response } from 'express';
import pool from '../../database';
import { Query } from '../../../query/query';
import userDao from '../dao/userDao'
import { Constants } from '../../../constant/Constant';


class UserController {

    public async listAllUser(req: Request, res: Response): Promise<any> {
        try {
            const list = await userDao.listAllUser();
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }
    }

    public async delete(req: Request, res: Response): Promise<any> {
        try {
            const { id } = req.params;
            const r: boolean = await userDao.delete(+id);
            if (r) {
                res.json({
                    status: Constants.SUCCESS,
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: 'FAIL',
                    message: 'error no eliminado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: 'FAIL',
                message: 'error respuesta servidor no eliminado',
                data: false
            });
        }
    }

    public async insertUser(req: Request, res: Response): Promise<any> {   
        const body = req.body;
        await pool.query(Query.INSERT_USER, [body.nameUser,
        body.lastNameUser, body.identUser, body.passUser, body.emailUser])
            .then((response: any) => {
                const rs = response;
                if (rs <= 0) {
                    return res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'insertado vacio',
                        data: true
                    });
                } else {
                    return res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            })
            .catch((err: any) => {
                console.log(err)
                return res.status(500).json({
                    status: 'FAIL',
                    message: 'error respuesta servidor no insertado',
                    data: false
                });
            })
            .finally(() => {
                //console.log('cerrar poool')
                //pool.end()
            })
    }

    public async updateUser(req: Request, res: Response): Promise<any> {     
        const body = req.body;
        await pool.query(Query.UPDATE_USER, [body.nameUser,
        body.lastNameUser, body.identUser, body.emailUser, body.idUser])
            .then((response: any) => {
                const rs = response;
                if (rs <= 0) {
                    return res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'actualizado vacio',
                        data: true
                    });
                } else {
                    return res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            })
            .catch((err: any) => {
                console.log(err)
                return res.status(500).json({
                    status: 'FAIL',
                    message: 'error respuesta servidor no actualizado',
                    data: false
                });
            })
            .finally(() => {
                //console.log('cerrar poool')
                //pool.end()
            })
    }

    public async listOptionValue(req: Request, res: Response): Promise<any> {      
        try {
            const list = await userDao.listOptionValue('cod_identification');
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'lista optionsValue ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        } 
    }
}
const userController = new UserController;
export default userController;