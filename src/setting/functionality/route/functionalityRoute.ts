import { Router } from "express";
import functionalityController from "../controller/functionalityController";


class FunctionalityRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {        
        this.router.get('/listSection', functionalityController.listAllSection);
      
        this.router.get('/view/:idSection/list', functionalityController.listAllFunctionality);
        this.router.post('/view/:idSection/functionality', functionalityController.insert);
        this.router.put('/view/:idSection/functionality', functionalityController.update);
        this.router.delete('/view/:idSection/functionality/:id', functionalityController.delete);

    }

}

export default new FunctionalityRoutes().router;