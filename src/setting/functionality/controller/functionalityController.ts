import { Request, Response } from 'express';
import pool from '../../database';
import { Query } from '../../../query/query';
import functionalityDao from '../dao/functionalityDao'
import { SystemFunctionality } from '../../../model/SystemFunctionality';
import { Constants } from '../../../constant/Constant';


class FunctionalityController {


    public async listAllFunctionality(req: Request, res: Response): Promise<any> {
        
        try {
            const { idSection } = req.params;    
            const list = await functionalityDao.listAllFunctionality(+idSection);
            res.status(200).json({
                status: 'SUCCESS',
                message : 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: 'FAIL',
                message: error.toString(),
                data: null
            });
        }    
    }

    public async listAllSection(req: Request, res: Response): Promise<any> {
        
        try {
            const list = await functionalityDao.listAllSection();
            res.status(200).json({
                status: Constants.SUCCESS,
                message : 'lista ',
                data: list
            });
        } catch (error) {            
            res.status(500).json({
                status: Constants.FAIL,
                message: error.toString(),
                data: null
            });
        }    
    }


    public async insert(req: Request, res: Response): Promise<any> {
       
        try {
        const opt:SystemFunctionality = req.body;
        const rs: boolean = await functionalityDao.insert(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async update(req: Request, res: Response): Promise<any> {
        
        try {
        const opt:SystemFunctionality = req.body;
        const rs: boolean = await functionalityDao.update(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            console.log(req.baseUrl);
            console.log(req.params);
            console.log('id option delete: ',id);
            const r: boolean = await functionalityDao.delete(+id);
            if (r) {
                res.json({
                    status: 'SUCCESS',
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: 'FAIL',
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: 'FAIL',
                message: err.toString(),
                data: false
            });
        }
    }




}
const functionalityController = new FunctionalityController;
export default functionalityController;
