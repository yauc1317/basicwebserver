import pool from '../../database';
import { Query } from '../../../query/query';
import { SystemOption } from '../../../model/SystemOption';
import { SystemFunctionality } from '../../../model/SystemFunctionality';
import { SystemSection } from '../../../model/SystemSection';


class FunctionalityDao {

 
    public async listAllFunctionality(idSection: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_FUNCTIONALITY_BY_SECTION,[idSection]);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemFunctionality = new SystemFunctionality();
                    obj.idFunctionality= item.id_functionality
                    obj.idSection= item.id_section
                    obj.sysIdFunctionality= item.sys_id_functionality
                    obj.codFunctionality= item.cod_functionality
                    obj.nameFunctionality= item.name_functionality
                    obj.descFunctionality= item.desc_functionality
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

    public async listAllSection(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_ALL_FUNCTIONALITY_SECTION);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemSection = new SystemSection();
                    obj.idSection= item.id_section                    
                    obj.nameSection= item.name_section
                    obj.descSection= item.desc_section                    
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

    public async insert(oV: SystemFunctionality): Promise<any> {
   
        var res = false;
        try {            
            await pool.query(Query.INSERT_FUNCTIONALITY, 
                [ oV.idSection, oV.sysIdFunctionality, oV.codFunctionality, oV.nameFunctionality, oV.descFunctionality]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async update(oV: SystemFunctionality): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.UPDATE_FUNCTIONALITY,
                 [oV.idSection, oV.sysIdFunctionality, oV.codFunctionality, oV.nameFunctionality, oV.descFunctionality, oV.idFunctionality]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }


    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_FUNCTIONALITY, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

}
const functionalityDao = new FunctionalityDao;
export default functionalityDao;
