import { Router } from "express";
import optionController from "../controller/optionController";
import optionValueRoute from "../../option-value/routes/optionValueRoute";
import optionValueController from "../../option-value/controller/optionValueController";

class OptionRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {        
        this.router.get('/listOption', optionController.listAllOption);
        this.router.post('/option', optionController.insert);
        this.router.put('/option', optionController.update);
        this.router.delete('/option/:id', optionController.delete);
        
        //rutas hijas para los valores de las opciones
        this.router.get('/view/:idOpt/list', optionValueController.listAllOption);
        this.router.get('/view/:codeOpt/listCode', optionValueController.listOptionByCode);
        this.router.post('/view/:idOpt/create', optionValueController.insert);
        this.router.put('/view/:idOpt/update', optionValueController.update);
        this.router.delete('/view/:idOpt/delete/:idOptVal', optionValueController.delete);
    }

}

export default new OptionRoutes().router;