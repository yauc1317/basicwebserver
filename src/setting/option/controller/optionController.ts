import { Request, Response } from 'express';
import pool from '../../database';
import { Query } from '../../../query/query';
import optionDao from '../dao/optionDao'
import { SystemOption } from '../../../model/SystemOption';


class OptionController {

    public async listAllOption(req: Request, res: Response): Promise<any> {
        try {
            const list = await optionDao.listAllOption();
            res.status(200).json({
                status: 'SUCCESS',
                message : 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: 'FAIL',
                message: error.toString(),
                data: null
            });
        }    
    }

    
    public async insert(req: Request, res: Response): Promise<any> {
        try {
        const opt:SystemOption = req.body;
        const rs: boolean = await optionDao.insert(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async update(req: Request, res: Response): Promise<any> {
        try {
        const opt:SystemOption = req.body;
        const rs: boolean = await optionDao.update(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: 'SUCCESS',
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: 'FAIL',
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            console.log(req.baseUrl);
            console.log(req.params);
            console.log('id option delete: ',id);
            const r: boolean = await optionDao.delete(+id);
            if (r) {
                res.json({
                    status: 'SUCCESS',
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: 'FAIL',
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: 'FAIL',
                message: err.toString(),
                data: false
            });
        }
    }




}
const optionController = new OptionController;
export default optionController;
