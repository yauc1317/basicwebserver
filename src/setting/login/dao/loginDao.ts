import pool from '../../database';
import {Query} from '../../../query/query';
import {AuthData} from '../../../model/AuthData';


class LoginDao {
    public async getUserByIdentPass(idUser: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.ENTERPRISE_BY_USER, [idUser]);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                return {
                    idInst: item.id_inst,
                    nameInst: item.name_inst,
                    nitInst: item.nit_inst,
                    usernameInst: item.username_inst
                };
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async listEnterpriseByIdUser(idUser: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.ENTERPRISE_BY_USER, [idUser]);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                return {
                    idInst: item.id_inst,
                    nitInst: item.nit_inst,
                    nameInst: item.name_inst,
                    stateInst: item.state_inst,
                    usernameInst: item.username_inst,
                    addressInst: item.address_inst,
                    contactInst: item.contact_inst,
                    misionInst: item.mision_inst,
                    visionInst: item.vision_inst,
                    sloganInst: item.slogan_inst,
                    logoInst: item.logo_inst,
                    imagesInst: item.images_inst,
                    codeMinisteryInst: item.code_ministery_inst,
                    legalRepreInst: item.legal_repre_inst,
                    kindIdentRepreInst: item.kind_ident_repre_inst,
                    numberRepreInst: item.number_repre_inst,
                    regimenType: item.regimen_type
                };
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async listEnterpriseByIdUser2(idUser: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.ENTERPRISE_BY_USER, [idUser]);
            const rs = rsw.rows;
            var list = [];
            list = rs.map((item: any) => {
                return {
                    idInst: item.id_inst,
                    nameInst: item.name_inst,
                    nitInst: item.nit_inst,
                    usernameInst: item.username_inst
                };
            });
        } catch (err) {
            console.log(err);
            throw err;
        }
        return list;
    }

    public async validateSectionByUser(data: AuthData): Promise<any> {
        var validate: boolean = false;
        console.log('validar section')
        console.log(data)
        try {
            var query: string;
            if (data.typeComponent === 'section') {
                query = Query.validateSectionByUserEnterprise;
            } else {
                query = Query.validateSectionByUserRol;
            }
            const rsw = await pool.query(query, [data.sectionA, data.idUser]);
            const rs = rsw.rows;
            if (rs[0].value != '0') {
                validate = true;
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
        return validate;
    }
}

const loginDao = new LoginDao;
export default loginDao;
