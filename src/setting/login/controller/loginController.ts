import { Request, Response, NextFunction } from 'express';
import pool from '../../database';
import { Query } from '../../../query/query';
import loginDao from '../dao/loginDao'
import { SystemUser } from "../../../model/SystemUser";
import { JsonWebTokenGen } from "../../../util/JsonWebTokenGen";
import { Constants } from "../../../constant/Constant";
import { AuthData } from "../../../model/AuthData";

require('../../configConstants');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

class LoginController {

    public async login(req: Request, res: Response): Promise<any> {
        await pool.query(Query.LOGIN_USER_PASSWORD, [req.body.identUser, req.body.pass])
            .then(async (response: { rows: any; }) => {
                const rs = response.rows;
                if (rs <= 0) {
                    console.log('usuario NO encontrado')
                    return res.status(200).json({
                        status: Constants.FAIL,
                        message: 'Usuario o contraseña equivocados',
                        data: null,
                        token: null,
                    });
                } else {
                    console.log('usuario SI encontrado')
                    const lstInstitutions = await loginDao.listEnterpriseByIdUser(rs[0].id_user);
                    var user = new SystemUser();
                    user.idUser = rs[0].id_user;
                    user.idSpeciality = rs[0].id_speciality;
                    user.nameUser = rs[0].name_user;
                    user.lastNameUser = rs[0].last_name_user;
                    user.identUser = rs[0].ident_user;
                    user.emailUser = rs[0].email_user;
                    user.activeUser = rs[0].active_user;
                    user.configProfileUser = rs[0].config_profile_user;
                    user.idProfile = rs[0].id_profile;
                    user.phoneUser = rs[0].phone_user;
                    user.lstInstitutions = lstInstitutions;
                    const token2 = JsonWebTokenGen.getToken(user);
                    return res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'login exitoso',
                        data: user,
                        token: token2
                    });
                }
            })
            .catch((err: any) => {
                console.log(err)
                return res.status(500).json({
                    status: Constants.FAIL,
                    message: 'error respuesta servidor',
                    data: null,
                    token: null,
                });
            })
            .finally(() => {
                console.log('cerrar poool')
                //pool.end()
            })
    }

    public async verifyTokenAndSection(req: Request, res: Response): Promise<any> {
        const token2 = JsonWebTokenGen.getNewToken(req.params);
        console.log('body ', req.body);
        var valid: boolean = false;
        if (req.body.sectionA === Constants.HOME) {
            console.log('entro a home')
            valid = true;
        } else {
            const authData: AuthData = req.params;
            authData.sectionA = req.body.sectionA[0];
            authData.typeComponent = req.body.typeComponent;
            valid = await loginDao.validateSectionByUser(authData);
        }
        console.log('---respuesta permiso',valid)
        return res.status(200).json({
            status: Constants.SUCCESS,
            data: valid,
            token: token2
        });

    }

    public async validateTokenInServer(req: Request, res: Response): Promise<any> {
        const token2 = JsonWebTokenGen.getNewToken(req.params);
        if (req.params.idInst === null || req.params.idInst === '0') {
            return res.status(401).json({
                status: 'FAIL',
                message: 'No tiene empresa seleccionada',
                data: false,
                token: null,
            })
        }
        return res.status(200).json({
            status: 'SUCCESS',
            data: true,
            token: token2
        });
    }


}

const loginController = new LoginController;
export default loginController;
