import {Router} from "express";
import loginController from "../controller/loginController";
import {JsonWebTokenGen} from "../../../util/JsonWebTokenGen";


class LoginRoutes {
    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.post('/loginAuth', loginController.login);
        this.router.post('/verifyTokenAndSection', JsonWebTokenGen.verifyToken, loginController.verifyTokenAndSection);
        this.router.post('/validateTokenInServer', JsonWebTokenGen.verifyToken, loginController.validateTokenInServer);
    }
}

export default new LoginRoutes().router;
