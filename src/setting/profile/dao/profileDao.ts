import pool from '../../database';
import { Query } from '../../../query/query';
import { SystemProfile } from '../../../model/SystemProfile';


class ProfileDao {

 
    public async listAllProfile(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_ALL_PROFILE);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemProfile = new SystemProfile();
                    obj.idProfile= item.id_profile
                    obj.nameProfile= item.name_profile
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

    public async insert(oV: SystemProfile): Promise<any> {
   
        var res = false;
        try {
            await pool.query(Query.INSERT_PROFILE, 
                [ oV.nameProfile]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async update(oV: SystemProfile): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.UPDATE_PROFILE,
                 [oV.nameProfile,  oV.idProfile]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }


    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_PROFILE, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

}
const profileDao = new ProfileDao;
export default profileDao;
