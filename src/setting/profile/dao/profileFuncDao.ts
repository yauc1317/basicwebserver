import pool from '../../database';
import { Query } from '../../../query/query';
import { SystemProfile } from '../../../model/SystemProfile';
import { SystemProfileFunc } from '../../../model/SystemProfileFunc';
import { SystemFunctionality } from '../../../model/SystemFunctionality';
import { isNullOrUndefined } from 'util';
import { SystemSection } from '../../../model/SystemSection';


class ProfileFuncDao {

 
    public async listAllProfileFunc(idProfile: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_ALL_PROFILE_FUNC,[idProfile]);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemProfileFunc = new SystemProfileFunc();
                    obj.idProfFunc = item.id_prof_func
                    obj.idProfile= item.id_profile
                    obj.idFunctionality= item.id_functionality
                    const objF:SystemFunctionality = new SystemFunctionality();
                    objF.idFunctionality = item.id_functionality
                    objF.idSection = item.id_section
                    objF.sysIdFunctionality = item.sys_id_functionality
                    objF.codFunctionality = item.cod_functionality
                    objF.nameFunctionality = item.name_functionality
                    objF.descFunctionality = item.desc_functionality
                    obj.systemFunctionality = objF
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

    public async listInnerProfileFunc(idProfile: number): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_INNER_PROFILE_FUNC,[idProfile]);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemProfileFunc = new SystemProfileFunc();
                    obj.idProfFunc = item.id_prof_func
                    obj.idProfile= item.id_profile
                    obj.idFunctionality= item.id_functionality
                    if (obj.idProfFunc!=undefined) {
                        obj.checked = true;
                    } else {
                        obj.checked = false;
                    }
                    const objF:SystemFunctionality = new SystemFunctionality();
                    objF.idFunctionality = item.id_functionality
                    objF.idSection = item.id_section
                    objF.sysIdFunctionality = item.sys_id_functionality
                    objF.codFunctionality = item.cod_functionality
                    objF.nameFunctionality = item.name_functionality
                    objF.descFunctionality = item.desc_functionality
                    obj.systemFunctionality = objF
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

    public async insert(list: Array<SystemProfileFunc>, idProfile:number,arrNotDel:string): Promise<any> {
        console.log('----dao insert list')
           
        var res = false;
        try {
            await pool.query('BEGIN')
            //eliminar todos
            console.log(Query.DELETE_PROFILE_FUNC_UNCHECK.replace('array',arrNotDel))
            await pool.query(Query.DELETE_PROFILE_FUNC_UNCHECK.replace('array',arrNotDel), [idProfile]);
            for(let i of list){
            await pool.query(Query.INSERT_PROFILE_FUNC, [ i.idProfile, i.idFunctionality, i.idProfile, i.idFunctionality]);             
            }
            await pool.query('COMMIT')
            res = true;
        } catch (err) {
            await pool.query('ROLLBACK')
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async update(o: SystemProfileFunc): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.UPDATE_PROFILE,
                 [o.idProfile, o.idFunctionality, o.idProfFunc]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }


    public async delete(id: number): Promise<any> {
        var res = false;
        try {
            await pool.query(Query.DELETE_PROFILE, [id]);
            res = true;
        } catch (err) {
            res = false;
            console.log(err);
            throw err;
        }
        return res;
    }

    public async listAllSection(): Promise<any> {
        try {
            const rsw = await pool.query(Query.LIST_ALL_FUNCTIONALITY_SECTION);
            const rs = rsw.rows;
            var list = [];         
                list = rs.map((item: any) => {
                    const obj:SystemSection = new SystemSection();
                    obj.idSection= item.id_section                    
                    obj.nameSection= item.name_section
                    obj.descSection= item.desc_section                    
                    return obj;           
                });               
        } catch (err) {           
            console.log(err);
            throw err;
        }  
        return list;
    }

}
const profileFuncDao = new ProfileFuncDao;
export default profileFuncDao;
