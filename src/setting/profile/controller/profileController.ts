import { Request, Response } from 'express';
import pool from '../../database';
import { Query } from '../../../query/query';
import profileDao from '../dao/profileDao'
import profileFuncDao from '../dao/profileFuncDao'
import { SystemOption } from '../../../model/SystemOption';
import { Constants } from '../../../constant/Constant';
import { SystemProfile } from '../../../model/SystemProfile';


class ProfileController {


    public async listAllProfile(req: Request, res: Response): Promise<any> {
        
        try {
            const list = await profileDao.listAllProfile();
            res.status(200).json({
                status: Constants.SUCCESS,
                message : 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.ERROR,
                message: error.toString(),
                data: null
            });
        }    
    }

    
    public async insert(req: Request, res: Response): Promise<any> {
       
        try {
        const opt:SystemProfile = req.body;
        const rs: boolean = await profileDao.insert(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'insertado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: Constants.ERROR,
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async update(req: Request, res: Response): Promise<any> {
        
        try {
        const opt:SystemProfile = req.body;
        const rs: boolean = await profileDao.update(opt);            
                if (rs) {                
                     res.status(200).json({
                        status: Constants.SUCCESS,
                        message: 'actualizado correctamente ',
                        data: true
                    });
                }
            } catch(err){
                console.log(err)
                 res.status(500).json({
                    status: Constants.FAIL,
                    message: err.toString(),
                    data: false
                });
            }           
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            console.log(req.baseUrl);
            console.log(req.params);
            console.log('id option delete: ',id);
            const r: boolean = await profileDao.delete(+id);
            if (r) {
                res.json({
                    status: Constants.SUCCESS,
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: Constants.ERROR,
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: Constants.ERROR,
                message: err.toString(),
                data: false
            });
        }
    }




}
const profileController = new ProfileController;
export default profileController;
