import { Request, Response } from 'express';
import profileFuncDao from '../dao/profileFuncDao'
import { Constants } from '../../../constant/Constant';
import { SystemProfileFunc } from '../../../model/SystemProfileFunc';
import { SystemSection } from '../../../model/SystemSection';
import { Directory } from '../model/Directory';
import { SystemFunctionality } from '../../../model/SystemFunctionality';


class ProfileFuncController {


    public async listAllProfileFunc(req: Request, res: Response): Promise<any> {

        try {
            const { idProfile } = req.params;
            const list = await profileFuncDao.listInnerProfileFunc(+idProfile);
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'lista ',
                data: list
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.ERROR,
                message: error.toString(),
                data: null
            });
        }
    }


    public async getTreeViewProfileFunc(req: Request, res: Response): Promise<any> {

        try {
            const directories: Directory[] = [];
            const { idProfile } = req.params;
            const listSection = await profileFuncDao.listAllSection();
            const list = await profileFuncDao.listInnerProfileFunc(+idProfile);
            listSection.map((item: SystemSection) => {
                if (item != undefined) {
                    const node = new Directory(item.nameSection, [], [], item);
                    directories.push(node)
                }
            });

            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'arbol ',
                data: directories
            });
        } catch (error) {
            console.log('exception controller')
            console.log(error.toString())
            res.status(500).json({
                status: Constants.ERROR,
                message: error.toString(),
                data: null
            });
        }
    }


    // Example usage
    public async unflatten(req: Request, res: Response): Promise<any> {
        try {
            const { idProfile } = req.params;
            const listFunc = await profileFuncDao.listInnerProfileFunc(+idProfile);
        
           
           const list = listFunc.map((item: any) => {
                const obj:SystemProfileFunc = new SystemProfileFunc();
                obj.idProfFunc = item.idProfFunc
                obj.idProfile = item.idProfile
                obj.idFunctionality = item.idFunctionality
                if (obj.idProfFunc!=undefined) {
                    obj.checked = true;
                } else {
                    obj.checked = false;
                }
                const objF:SystemFunctionality = new SystemFunctionality();
                objF.idFunctionality = item.systemFunctionality.idFunctionality
                objF.idSection = item.systemFunctionality.idSection
                objF.sysIdFunctionality = item.systemFunctionality.sysIdFunctionality
                objF.codFunctionality = item.systemFunctionality.codFunctionality
                objF.nameFunctionality = item.systemFunctionality.nameFunctionality
                objF.descFunctionality = item.systemFunctionality.descFunctionality
                obj.systemFunctionality = objF               
                const dir: Directory = new Directory(obj.systemFunctionality.nameFunctionality, [], [], obj);               
                return dir;           
            });      

            var map = {}, node: Directory, roots = [], i;
            for (i = 0; i < list.length; i += 1) {
                map[list[i].data.idFunctionality] = i; // initialize the map
                list[i].directories = []; // initialize the children
            }
            for (i = 0; i < list.length; i += 1) {
                node = list[i];
                if (node.data.systemFunctionality.sysIdFunctionality !== null) {
                    // if you have dangling branches check that map[node.parentId] exists                  
                    list[map[node.data.systemFunctionality.sysIdFunctionality]].directories.push(node);
                } else {
                    roots.push(node);
                }
            }
            res.status(200).json({
                status: Constants.SUCCESS,
                message: 'ok',
                data: roots
            });

        } catch (err) {
            console.log(err)
            res.status(500).json({
                status: Constants.ERROR,
                message: err.toString(),
                data: false
            });
        }


    }


    public async insert(req: Request, res: Response): Promise<any> {

        try {
            console.log('----logic insert list')
           
            
            const list: Array<SystemProfileFunc> = req.body;
            const idProfile = req.params.idProfile
             //falta pasar lista para no eliminar
            var arrDel:string='';
        
            if (list.length>0) {
                list.map((item: SystemProfileFunc) => {
                    arrDel = arrDel+item.idFunctionality+','
               });
               arrDel= arrDel.slice(0,arrDel.length-1) 
            } else {
                arrDel='0'
            }
            //fin cadena eliminar

            const rs: boolean = await profileFuncDao.insert(list, +idProfile,arrDel);
            if (rs) {
                res.status(200).json({
                    status: Constants.SUCCESS,
                    message: 'insertado correctamente ',
                    data: true
                });
            }
        } catch (err) {
            console.log(err)
            res.status(500).json({
                status: Constants.ERROR,
                message: err.toString(),
                data: false
            });
        }
    }

    arrayStringDeleteFunct(list: Array<SystemProfileFunc>): string{
        console.log('---dentro aradelete---')
        var arrDel:string='';
        
        if (list.length>0) {
            list.map((item: SystemProfileFunc) => {
                arrDel = arrDel+item.idFunctionality+','
           });
           arrDel.slice(0,arrDel.length-1) 
        } else {
            arrDel='0'
        }
        
        return arrDel
    }

    public async update(req: Request, res: Response): Promise<any> {

        try {
            const opt: SystemProfileFunc = req.body;
            const rs: boolean = await profileFuncDao.update(opt);
            if (rs) {
                res.status(200).json({
                    status: Constants.SUCCESS,
                    message: 'actualizado correctamente ',
                    data: true
                });
            }
        } catch (err) {
            console.log(err)
            res.status(500).json({
                status: Constants.FAIL,
                message: err.toString(),
                data: false
            });
        }
    }

    public async delete(req: Request, res: Response): Promise<void> {
        try {
            const { id } = req.params;
            console.log(req.baseUrl);
            console.log(req.params);
            console.log('id option delete: ', id);
            const r: boolean = await profileFuncDao.delete(+id);
            if (r) {
                res.json({
                    status: Constants.SUCCESS,
                    message: 'eliminado correctamente',
                    data: true
                });
            } else {
                res.status(500).json({
                    status: Constants.ERROR,
                    message: 'no encontrado',
                    data: false
                });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({
                status: Constants.ERROR,
                message: err.toString(),
                data: false
            });
        }
    }




}
const profileFuncController = new ProfileFuncController;
export default profileFuncController;
