import { Router } from "express";
import profileController from "../controller/profileController";
import profileFuncController from "../controller/profileFuncController";
class ProfileRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {        
        this.router.get('/list', profileController.listAllProfile);
        this.router.post('/profile', profileController.insert);
        this.router.put('/profile', profileController.update);
        this.router.delete('/profile/:id', profileController.delete);
        
        //rutas hijas para los valores de las opciones
        this.router.get('/view/:idProfile/list', profileFuncController.listAllProfileFunc);
        this.router.get('/view/:idProfile/tree', profileFuncController.unflatten);
        this.router.post('/view/:idProfile/profileFunc', profileFuncController.insert);
        this.router.put('/view/:idProfile/profileFunc', profileFuncController.update);
        this.router.delete('/view/:idProfile/profileFunc/:idProfFunc', profileFuncController.delete);
    }

}

export default new ProfileRoutes().router;