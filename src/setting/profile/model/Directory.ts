import { SystemProfileFunc } from "../../../model/SystemProfileFunc";

export class Directory{
    name?: string;
    directories: Array<Directory>;
    files?: Array<any>;
    expanded?:boolean;
    checked?:boolean;
    data?:SystemProfileFunc;
    constructor(name: string,directories: any[],files: any[],data: SystemProfileFunc) {
        this.name = name;
        this.files = files;
        this.directories = directories
        this.expanded = false;
        this.checked = false;
        this.data = data;
    }

    toggle(){
        this.expanded = !this.expanded;
    }
    check(){
        //let newState = !this.checked;
        //this.checked = newState;
        this.checked = !this.checked;             
        this.checkRecursive(this.checked);
    }
    checkRecursive(state: boolean){
        this.directories.forEach(d => {
            d.checked = state;           
            d.checkRecursive(state);
        })
       
    }
}