import {SystemInstitution} from "./SystemInstitution";

export class SystemUser {
    idUser?: number;
    idSpeciality?: number;
    nameUser?: number;
    lastNameUser?: boolean;
    identUser?: string;
    passUser?: string;
    emailUser?: string;
    activeUser?: boolean;
    configProfileUser?: string;
    idProfile?: number;
    phoneUser?: string;
    lstInstitutions?: SystemInstitution[];
}
