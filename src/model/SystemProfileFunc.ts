import { SystemFunctionality } from "./SystemFunctionality";

export class SystemProfileFunc {
    idProfFunc?: number;
    idProfile?: number;
    idFunctionality?: number;
    systemFunctionality: SystemFunctionality = new SystemFunctionality;
    checked?: boolean;
  }