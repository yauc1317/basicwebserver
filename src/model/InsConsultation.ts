export class InsConsultation {
    idCons?: number;
    idDocOffice?: number;
    idPH?: number;
    idPI?: number;
    dateCons?: Date;
    stateCons?: string;

    assignDate?: Date;
    idUserAssign?: number;
    cancelDate?: Date;
    idUserCancel?: number;
    desiredDate?: Date;

    meansRequest?: number;
    typeService?: number;
    idSpeciality?: number;
    
}