import {NextFunction, Request, Response} from "express";

const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
import {SystemUser} from "../model/SystemUser";
import {AuthData} from "../model/AuthData";
import { Constants } from "../constant/Constant";


export class JsonWebTokenGen {

    readonly SECURITY_KEY: string = 'halkeyholamundo';

    static getToken(user: SystemUser) {
        let idInst: any = 0;
        let usernameInst: any = '';
       // console.log(user.lstInstitutions);
        if (user.lstInstitutions != null && user.lstInstitutions.length == 1) {
            idInst = user.lstInstitutions[0].idInst;
            usernameInst = user.lstInstitutions[0].usernameInst;
        }

        const authData: AuthData = new AuthData();
        authData.identUser = user.identUser;
        authData.idUser = user.idUser;
        authData.idInst = idInst;
        authData.usernameInst = usernameInst;
        return jwt.sign({data: authData}, process.env.SEED, {
            expiresIn: process.env.CADUCIDAD_TOKEN //media hora
        });
    }

    static getNewToken(authData: AuthData) {
        return jwt.sign({data: authData}, process.env.SEED, {
            expiresIn: process.env.CADUCIDAD_TOKEN //media hora
        });
    }

    /*    const data = {
            ident_user: user.identUser,
            id_user: user.idUser,
            id_inst: idInst,
            username_inst: usernameInst
        };
        console.log(data);
        return jwt.sign({data: data}, process.env.SEED, {
            algorithm: 'RS256',
            expiresIn: process.env.CADUCIDAD_TOKEN, //media hora
            subject: user.idUser
        });
    }
    static getNewToken(token: string) {
         // var user: SystemUser = null;
         var decoded = jwt.verify(token, process.env.SEED);
         console.log(decoded);
        /*Claims claims = Jwts.parser()
            .setSigningKey(SECURITY_KEY)
            .parseClaimsJws(token).getBody();

        if (claims.getExpiration().after(new Date())) {
            user = new SystemUser();
            user.setIdentUser((String) claims.get("ident_user"));
            user.setIdUser((int) claims.get("id_user"));
            */

    static verifyToken = (req: Request, res: Response, next: NextFunction) => {
        let token = req.get('tokenAuth');
        jwt.verify(token, process.env.SEED, (err: any, decoded: any) => {
            if (err) {
                return res.status(200).json({
                    status: Constants.FAIL,
                    message: 'Token no valido: ' + err,
                    data: false,
                    token: null,
                })
            }
            req.params = decoded.data;
            console.log('verifytoken');
            next();
        });
    };


}
