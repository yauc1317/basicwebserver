export class Query {
    static SELECT_CONFIG_PARAM: string = 'SELECT * FROM config.ecnt_configuration_params where id_enterprise = $1';
    static SELECT_DATE: string = "SELECT NOW()";
    //login
    static LOGIN_USER_PASSWORD: string = 'select * from main_schema.system_user where ident_user = $1 and pass_user = $2';
    static enterpisesByUser: string = 'select si.* from main_schema.user_institution ui inner join main_schema.system_institution si ON si.id_inst = ui.id_inst where ui.id_user = $1 and si.state_inst = true';
    static validateSectionByUserEnterprise: string = "select count(*) as value from main_schema.user_institution ui inner join main_schema.system_institution ins ON ins.id_inst = ui.id_inst inner join main_schema.institution_section ise ON ise.id_inst = ins.id_inst inner join main_schema.system_section se ON se.id_section = ise.id_section where se.cod_section = $1 and ui.id_user = $2";
    static validateSectionByUserRol: string = "select count(*) as value from main_schema.user_institution ui inner join main_schema.system_user su ON su.id_user = ui.id_user inner join main_schema.system_profile sp ON sp.id_profile = su.id_profile inner join main_schema.system_profile_funct pf ON pf.id_profile = sp.id_profile inner join main_schema.system_functionality fu ON fu.id_functionality = pf.id_functionality where fu.cod_functionality = $1 and ui.id_user = $2";
    //option (parametros)
    static ENTERPRISE_BY_USER: string = 'select si.* from main_schema.user_institution ui inner join main_schema.system_institution si ON si.id_inst = ui.id_inst where ui.id_user = $1 and si.state_inst = true';
    //option
    static LIST_ALL_OPTION: string = 'SELECT * FROM main_schema.system_option;';
    static INSERT_OPTION: string = 'INSERT INTO main_schema.system_option(name_opt, desc_opt, cod_opt) VALUES ( $1, $2, $3);';
    static UPDATE_OPTION: string = 'UPDATE main_schema.system_option SET  name_opt=$1, desc_opt=$2, cod_opt=$3 WHERE id_opt=$4;';
    static DELETE_OPTION: string = 'DELETE FROM main_schema.system_option WHERE id_opt=$1;';
    //option_value
    static LIST_ALL_OPTION_VALUE: string = 'SELECT * FROM main_schema.system_option_value WHERE id_opt=$1;';
    static LIST_OPTION_VALUE_BY_CODE: string = 'SELECT op.* FROM main_schema.system_option o inner join main_schema.system_option_value op on o.id_opt = op.id_opt where op.state_opt_val=true and o.cod_opt=$1 ORDER BY op.name_opt_val';
    static INSERT_OPTION_VALUE: string = 'INSERT INTO main_schema.system_option_value(id_opt, name_opt_val, desc_opt_val, state_opt_val, cod_opt_val) VALUES ( $1, $2, $3, $4, $5);';
    static UPDATE_OPTION_VALUE: string = 'UPDATE main_schema.system_option_value SET  name_opt_val=$1, desc_opt_val=$2, state_opt_val=$3, cod_opt_val=$4 WHERE id_opt_val=$5;';
    static DELETE_OPTION_VALUE: string = 'DELETE FROM main_schema.system_option_value WHERE id_opt_val=$1;';
    //user
    static LIST_ALL_USER: string = "SELECT * FROM main_schema.system_user ORDER BY id_user";
    static DELETE_USER: string = "DELETE FROM main_schema.system_user WHERE id_user = $1";
    static INSERT_USER: string = "INSERT INTO main_schema.system_user(id_speciality, id_profile, name_user, last_name_user, ident_user, pass_user, email_user, active_user, config_profile_user, phone_user) VALUES (1, 111, $1, $2, $3, $4, $5, true, 'h' , '123')";
    static UPDATE_USER: string = "UPDATE main_schema.system_user SET name_user = $1, last_name_user = $2, ident_user = $3, email_user = $4 WHERE id_user = $5";
    //Speciality
    static LIST_ALL_SPECIALITY: string = "SELECT * FROM main_schema.system_speciality ORDER BY id_speciality";
    //functionality (permisos)
    static LIST_ALL_FUNCTIONALITY_SECTION: string = 'SELECT * FROM main_schema.system_section;';
    static LIST_FUNCTIONALITY_BY_SECTION: string = 'SELECT * FROM main_schema.system_functionality WHERE id_section=$1;';
    static INSERT_FUNCTIONALITY: string = 'INSERT INTO main_schema.system_functionality(id_section, sys_id_functionality, cod_functionality, name_functionality, desc_functionality) VALUES ( $1, $2, $3,$4,$5);';
    static UPDATE_FUNCTIONALITY: string = 'UPDATE main_schema.system_functionality SET id_section=$1, sys_id_functionality=$2, cod_functionality=$3, name_functionality=$4, desc_functionality=$5  WHERE id_functionality=$6;';
    static DELETE_FUNCTIONALITY: string = 'DELETE FROM main_schema.system_functionality WHERE id_functionality=$1;';
    // profile (rol)
    static LIST_ALL_PROFILE: string = 'SELECT * FROM main_schema.system_profile;';
    static INSERT_PROFILE: string = 'INSERT INTO main_schema.system_profile(name_profile) VALUES ( $1);';
    static UPDATE_PROFILE: string = 'UPDATE main_schema.system_profile SET name_profile=$1  WHERE id_profile=$2;';
    static DELETE_PROFILE: string = 'DELETE FROM main_schema.system_profile WHERE id_profile=$1;';
    /*
    // profile  (rol) - functionality
    static LIST_ALL_PROFILE_FUNC: string = 'SELECT * FROM main_schema.system_profile_funct pf INNER JOIN main_schema.system_functionality f ON  f.id_functionality = pf.id_functionality WHERE pf.id_profile = $1';
    static LIST_INNER_PROFILE_FUNC: string = 'SELECT pf.id_prof_func, pf.id_profile, pf.id_functionality, f.id_functionality, f.id_section, f.sys_id_functionality, f.cod_functionality, f.name_functionality, f.desc_functionality FROM main_schema.system_functionality f LEFT JOIN 	main_schema.system_profile_funct pf ON  f.id_functionality = pf.id_functionality and pf.id_profile =$1 ORDER BY f.id_section asc, f.sys_id_functionality desc, f.id_functionality asc';
    static INSERT_PROFILE_FUNC: string = 'INSERT INTO main_schema.system_profile_funct(id_profile, id_functionality) SELECT $1,$2 WHERE NOT EXISTS (select 1 from main_schema.system_profile_funct where id_profile=$3 and id_functionality=$4 ) ';
    static UPDATE_PROFILE_FUNC: string = 'UPDATE main_schema.system_profile SET name_profile=$1  WHERE id_profile=$2;';
    static DELETE_PROFILE_FUNC: string = 'DELETE FROM main_schema.system_profile_funct WHERE id_profile=$1;';
    static DELETE_PROFILE_FUNC_UNCHECK: string = 'DELETE FROM main_schema.system_profile_funct  WHERE id_profile = $1 and id_functionality not in (array)';
    //service
    static LIST_SERVICE: string = 'SELECT * FROM main_schema.system_service  ORDER BY id_serv asc;';
    static INSERT_SERVICE: string = 'INSERT INTO main_schema.system_service(name_serv, cups_serv, type_serv, active_priple_med, code_med, type_med, tradename_med, presentation_med, concentration_med, unit_measure_med, medicine_admin_med, price_med, type_presentation) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);';
    static UPDATE_SERVICE: string = 'UPDATE main_schema.system_service SET  name_serv=$1, cups_serv=$2, type_serv=$3, active_priple_med=$4, code_med=$5, type_med=$6, tradename_med=$7, presentation_med=$8, concentration_med=$9, unit_measure_med=$10, medicine_admin_med=$11, price_med=$12, type_presentation=$13 WHERE id_serv=$14;';
    static DELETE_SERVICE: string = 'DELETE FROM main_schema.system_service WHERE id_serv=$1;';
    //tariff_manual
    static LIST_TARIFF_MANUAL: string = 'SELECT * FROM main_schema.system_tariff_manual ORDER BY id_tama asc;';
    static INSERT_TARIFF_MANUAL: string = 'INSERT INTO main_schema.system_tariff_manual(name_tama) VALUES ( $1);';
    static UPDATE_TARIFF_MANUAL: string = 'UPDATE main_schema.system_tariff_manual SET  name_tama=$1 WHERE id_tama=$2;';
    static DELETE_TARIFF_MANUAL: string = 'DELETE FROM main_schema.system_tariff_manual WHERE id_tama=$1;';
    //section
    static LIST_SECTION: string = 'SELECT * FROM main_schema.system_section ORDER BY id_section asc';
    static INSERT_SECTION: string = 'INSERT INTO main_schema.system_section(name_section, desc_section) VALUES ( $1, $2);';
    static UPDATE_SECTION: string = 'UPDATE main_schema.system_section SET  name_section=$1, desc_section=$2 WHERE id_section=$3;';
    static DELETE_SECTION: string = 'DELETE FROM main_schema.system_section WHERE id_section=$1;';
    //eapb
    static LIST_EAPB: string = 'SELECT * FROM main_schema.system_eapb ORDER BY id_eapb asc';
    static INSERT_EAPB: string = 'INSERT INTO main_schema.system_eapb(name_eapb, cod_eapb, nit_eapb, legal_repre_eapb, phone_repre_eapb, kind_identification_repre_eapb) VALUES ( $1, $2, $3, $4, $5, $6);';
    static UPDATE_EAPB: string = 'UPDATE main_schema.system_eapb SET  name_eapb=?, cod_eapb=?, nit_eapb=?, legal_repre_eapb=?, phone_repre_eapb=?, kind_identification_repre_eapb=? WHERE id_eapb=?;';
    static DELETE_EAPB: string = 'DELETE FROM main_schema.system_eapb WHERE id_eapb=?;';
    //constant
    static LIST_CONSTANT: string = 'SELECT id_constant, c.id_inst, name_constant, value_constant, desc_constant,i.name_inst FROM main_schema.system_constant c LEFT JOIN main_schema.system_institution i ON i.id_inst = c.id_inst ORDER BY id_constant asc';
    static INSERT_CONSTANT: string = 'INSERT INTO main_schema.system_constant(id_inst, name_constant, value_constant, desc_constant) VALUES ($1, $2, $3, $4);';
    static UPDATE_CONSTANT: string = 'UPDATE main_schema.system_constant SET  id_inst=$1, name_constant=$2, value_constant=$3, desc_constant=$4 WHERE id_constant=$5;';
    static DELETE_CONSTANT: string = 'DELETE FROM main_schema.system_constant WHERE id_constant=$1;';
    static LIST_CONSTANT_INST: string = 'SELECT id_inst, nit_inst, name_inst FROM main_schema.system_institution WHERE state_inst=true;';
    */
    // profile  (rol) - functionality
    static LIST_ALL_PROFILE_FUNC: string = 'SELECT * FROM main_schema.system_profile_funct pf INNER JOIN main_schema.system_functionality f ON  f.id_functionality = pf.id_functionality WHERE pf.id_profile = $1';
    static LIST_INNER_PROFILE_FUNC: string = 'SELECT pf.id_prof_func, pf.id_profile, pf.id_functionality, f.id_functionality, f.id_section, f.sys_id_functionality, f.cod_functionality, f.name_functionality, f.desc_functionality FROM main_schema.system_functionality f LEFT JOIN 	main_schema.system_profile_funct pf ON  f.id_functionality = pf.id_functionality and pf.id_profile =$1 ORDER BY f.id_section asc, f.sys_id_functionality desc, f.id_functionality asc';
    static INSERT_PROFILE_FUNC: string = 'INSERT INTO main_schema.system_profile_funct(id_profile, id_functionality) SELECT $1,$2 WHERE NOT EXISTS (select 1 from main_schema.system_profile_funct where id_profile=$3 and id_functionality=$4 ) ';
    static UPDATE_PROFILE_FUNC: string = 'UPDATE main_schema.system_profile SET name_profile=$1  WHERE id_profile=$2;';
    static DELETE_PROFILE_FUNC: string = 'DELETE FROM main_schema.system_profile_funct WHERE id_profile=$1;';
    static DELETE_PROFILE_FUNC_UNCHECK: string = 'DELETE FROM main_schema.system_profile_funct  WHERE id_profile = $1 and id_functionality not in (array)';

    //service
    static LIST_SERVICE: string = 'SELECT * FROM main_schema.system_service  ORDER BY id_serv asc;';
    static INSERT_SERVICE: string = 'INSERT INTO main_schema.system_service(name_serv, cups_serv, type_serv, active_priple_med, code_med, type_med, tradename_med, presentation_med, concentration_med, unit_measure_med, medicine_admin_med, price_med, type_presentation) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);';
    static INSERT_SERVICE_CSV: string = 'INSERT INTO main_schema.system_service(name_serv, cups_serv, code_med, cups_group, upc_pos) VALUES ( $1, $2, $3, $4, $5);';
    static UPDATE_SERVICE: string = 'UPDATE main_schema.system_service SET  name_serv=$1, cups_serv=$2, type_serv=$3, active_priple_med=$4, code_med=$5, type_med=$6, tradename_med=$7, presentation_med=$8, concentration_med=$9, unit_measure_med=$10, medicine_admin_med=$11, price_med=$12, type_presentation=$13 WHERE id_serv=$14;';
    static DELETE_SERVICE: string = 'DELETE FROM main_schema.system_service WHERE id_serv=$1;';

    //tariff_manual
    static LIST_TARIFF_MANUAL: string = 'SELECT * FROM main_schema.system_tariff_manual ORDER BY id_tama asc;';
    static INSERT_TARIFF_MANUAL: string = 'INSERT INTO main_schema.system_tariff_manual(name_tama) VALUES ( $1);';
    static UPDATE_TARIFF_MANUAL: string = 'UPDATE main_schema.system_tariff_manual SET  name_tama=$1 WHERE id_tama=$2;';
    static DELETE_TARIFF_MANUAL: string = 'DELETE FROM main_schema.system_tariff_manual WHERE id_tama=$1;';

    //section
    static LIST_SECTION: string = 'SELECT * FROM main_schema.system_section ORDER BY id_section asc';
    static INSERT_SECTION: string = 'INSERT INTO main_schema.system_section(name_section, desc_section) VALUES ( $1, $2);';
    static UPDATE_SECTION: string = 'UPDATE main_schema.system_section SET  name_section=$1, desc_section=$2 WHERE id_section=$3;';
    static DELETE_SECTION: string = 'DELETE FROM main_schema.system_section WHERE id_section=$1;';

    //eapb
    static LIST_EAPB: string = 'SELECT * FROM main_schema.system_eapb ORDER BY id_eapb asc';
    static INSERT_EAPB: string = 'INSERT INTO main_schema.system_eapb(name_eapb, cod_eapb, nit_eapb, legal_repre_eapb, phone_repre_eapb, kind_identification_repre_eapb) VALUES ( $1, $2, $3, $4, $5, $6);';
    static UPDATE_EAPB: string = 'UPDATE main_schema.system_eapb SET  name_eapb=?, cod_eapb=?, nit_eapb=?, legal_repre_eapb=?, phone_repre_eapb=?, kind_identification_repre_eapb=? WHERE id_eapb=?;';
    static DELETE_EAPB: string = 'DELETE FROM main_schema.system_eapb WHERE id_eapb=?;';

    //constant
    static LIST_CONSTANT: string = 'SELECT id_constant, c.id_inst, name_constant, value_constant, desc_constant,i.name_inst FROM main_schema.system_constant c LEFT JOIN main_schema.system_institution i ON i.id_inst = c.id_inst ORDER BY id_constant asc';
    static INSERT_CONSTANT: string = 'INSERT INTO main_schema.system_constant(id_inst, name_constant, value_constant, desc_constant) VALUES ($1, $2, $3, $4);';
    static UPDATE_CONSTANT: string = 'UPDATE main_schema.system_constant SET  id_inst=$1, name_constant=$2, value_constant=$3, desc_constant=$4 WHERE id_constant=$5;';
    static DELETE_CONSTANT: string = 'DELETE FROM main_schema.system_constant WHERE id_constant=$1;';
    static LIST_CONSTANT_INST: string = 'SELECT id_inst, nit_inst, name_inst FROM main_schema.system_institution WHERE state_inst=true;';

    //ins_contract
    static LIST_CONTRACT: string = 'SELECT * FROM schema_institution.ins_contract ORDER BY id_cont asc;';
    static LIST_CONTRACT_LAZY: string = 'SELECT * FROM schema_institution.ins_contract filterPage ORDER BY id_cont asc LIMIT $1 OFFSET $2;';
    static INSERT_CONTRACT: string = 'INSERT INTO schema_institution.ins_contract(id_eapb, name_cont, intial_date_cont, final_date_cont, regime_cont, kind_cont, code_cont, state_cont, total_value_contract, id_tama, ajust_tama) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);';
    static INSERT_CONTRACT_OTHER: string = 'INSERT INTO schema_institution.ins_contract(id_eapb, name_cont, intial_date_cont, final_date_cont, regime_cont, kind_cont, code_cont, state_cont, total_value_contract, id_tama, ajust_tama,ins_id_cont) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING id_cont,name_cont;';
    static UPDATE_CONTRACT: string = 'UPDATE schema_institution.ins_contract SET id_eapb=$1, name_cont=$2, intial_date_cont=$3, final_date_cont=$4,regime_cont=$5, kind_cont=$6, code_cont=$7, state_cont=$8, total_value_contract=$9, id_tama=$10, ajust_tama=$11 WHERE id_cont=$12;';
    static DELETE_CONTRACT: string = '';
    static COUNT_CONTRACT_LAZY: string = 'SELECT count(*) FROM schema_institution.ins_contract filterPage';




    //ins_serv_contract
    //static LIST_INNER_SERVICE_CONTRACT: string='SELECT s.*, sc.id_serv_cont, sc.id_cont,  sc.id_tama, sc.real_value_serv, sc.tariff_adjustment, sc.total_goal_service FROM main_schema.system_service s LEFT JOIN 	schema_institution.ins_serv_contract sc ON s.id_serv = sc.id_serv and sc.id_cont =$1 ORDER BY  s.name_serv asc';
    static LIST_INNER_SERVICE_CONTRACT: string = 'SELECT sc.id_cont,  sc.id_serv_cont, sc.id_tama, ts.id_tama_serv, s.*, sc.real_value_serv, sc.tariff_adjustment, sc.total_goal_service, ts.value_tama_serv,ts.iva_value,ts.cod_tama,ts.descrip_tama,ts.points_tama,tm.name_tama FROM main_schema.system_service s LEFT JOIN 	schema_institution.ins_serv_contract sc ON s.id_serv = sc.id_serv and sc.id_cont =$1 left JOIN 	main_schema.sys_tama_serv ts ON ts.id_tama = sc.id_tama and sc.id_serv= ts.id_serv left JOIN 	main_schema.system_tariff_manual tm on tm.id_tama = ts.id_tama ORDER BY  s.id_serv asc';
    static DELETE_SERVICE_CONTRACT: string = 'DELETE FROM schema_institution.ins_serv_contract WHERE id_serv_cont = $1';
    static INSERT_UPDATE_SERVICE_CONTRACT: string = '';
    static INSERT_SERVICE_CONTRACT: string = 'INSERT INTO schema_institution.ins_serv_contract( id_cont, id_serv, id_tama, real_value_serv, tariff_adjustment, total_goal_service) VALUES ( $1, $2, $3, $4, $5, $6);';
    static UPDATE_SERVICE_CONTRACT: string = 'UPDATE schema_institution.ins_serv_contract SET  id_tama=$1, real_value_serv=$2, tariff_adjustment=$3 WHERE id_serv_cont=$4;';
    static GET_VALUE_SERVICE_TARIFF_MANUAL: string = 'SELECT   value_tama_serv FROM main_schema.sys_tama_serv where id_serv=$1 AND id_tama=$2 limit 1';

    //config diary (configuracion de agenda)
    static LIST_DIARY_DOCTOR_BY_PLACE_ATTENTION: string = 'SELECT u.id_user,u.name_user,u.last_name_user,u.ident_user,u.email_user,u.active_user, u.phone_user,d.id_speciality,d.professional_card,d.speciality FROM main_schema.system_user u INNER JOIN main_schema.system_speciality d on d.id_speciality = u.id_speciality LEFT JOIN schema_institution.ins_consultation c  on c.id_speciality= d.id_speciality LEFT JOIN schema_institution.ins_doctor_office dof on dof.id_doc_office = c.id_doc_office LEFT JOIN schema_institution.ins_place_attention pa on pa.id_place_attention = dof.id_place_attention WHERE pa.id_place_attention=$1 GROUP BY d.id_speciality,u.id_user ORDER BY u.id_user DESC ';
    static listDayWithDiaryByDoctorAndPlaceAttentionAllMonth: string = 'SELECT (EMA.date_cons::Date)::text as days, count(EMA.date_cons::Date) as total_cons FROM schema_institution.ins_consultation  EMA INNER JOIN schema_institution.ins_doctor_office ECR ON EMA.id_doc_office = ECR.id_doc_office INNER JOIN schema_institution.ins_place_attention EH ON ECR.id_place_attention = EH.id_place_attention WHERE ECR.id_place_attention =$1 AND EMA.id_speciality =$2 AND EMA.date_cons >=$3  AND EMA.date_cons <$4 GROUP BY days ORDER BY days asc';
    static listConsultByDoctorAndPlaceAttentionAllDay: string = 'SELECT EMA.*, (EMA.date_cons::Date)::text as days,(EMA.date_cons::Time) as hora FROM schema_institution.ins_consultation  EMA INNER JOIN schema_institution.ins_doctor_office ECR ON EMA.id_doc_office = ECR.id_doc_office INNER JOIN schema_institution.ins_place_attention EH ON ECR.id_place_attention = EH.id_place_attention WHERE ECR.id_place_attention =$1 AND EMA.id_speciality =$2 AND EMA.date_cons >=$3  AND EMA.date_cons <$4 ORDER BY days asc';
    static insertListInsConsultation: string = 'INSERT INTO schema_institution.ins_consultation(id_doc_office, date_cons, state_cons,id_speciality) VALUES ($1, $2, $3, $4);';


} 
